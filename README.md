# DSL vs API Experiment
This exercise aims to compare the process of writing BISM transformers in two different modes:
1. Utilizing the **[API](./api-doc/README.md)**. 
2. Utilizing the **[DSL](./dsl-doc/README.md)**.

Taken from [CRV 2014](https://link.springer.com/article/10.1007/s10009-017-0454-5#Sec36),  you are given 4 properties to monitor over FiTS (Financial Transaction System) which is a simplified financial transaction system we use as our base program. 
The monitor code is provided.

Your task is to write 8 transformers (one for each property and mode) to instrument the base program and extract events to the monitor.
For each transformer, we ask you to report some [metrics](#metrics).

You do need to worry about compiling the monitor and executing BISM, the repo contains scripts to do that.

 
## FiTS Monitoring

Our goal is to instrument FiTS to monitor four properties. For each property, we need to extract the events, monitor the property, and report a violation if it occurs. **The monitor code is already provided**, your task is to focus on the extraction of events from the base program.

Below, we provide the properties in a guarded-command language (GCL) as a series of Java-based rules in the format:

`event | condition → action.`

If the event occurs and the condition is satisfied, then action should be triggered.

In what follows:
- The term `target` refers to the object or program entity that is being monitored for compliance with a given property. 
- Patterns such as `*.openSession(..)` specify a wildcard `*` for the method name and arguments, meaning any method call matching the pattern should be monitored. The `..` notation is used to specify any number of arguments of any type. 

<!-- ### 1. **Only users based in Argentina can be Gold users.** 

This property can be expressed as follows:

```java
*.makeGoldUser(..) target (UserInfo u) | !(u.getCountry().equals("Argentina")) → fail("P1 violated");
```

-   The instrumentation must capture calls to `makeGoldUser` of the `UserInfo` class and send the target object to the monitor. 

- The monitor checks if `UserInfo u` object `u.getCountry().equals("Argentina")` and reports a violation accordingly. -->

### 1. **The transaction system must be initialised before any user logs in.**

The property states that the transaction system must be initialised before any user logs in. The property can be expressed as follows:

```java
TransactionSystem.initialised = .. | → Monitor.initialised = true;
UserInfo.openSession(..) | !Monitor.initialised → fail("P2 violated");
```

`Monitor Description`: The monitor sets an initialised flag to true when the system is initialised and check this flag upon the start of any user session. Hence, a trace containing a call to openSession before initializing violates the property while any other trace satisfies it.

`Instrumentation Description`: (TODO)
- Capture Fieldset to `* TransactionSystem.initialised` and before the method calls to `* UserInfo.openSession(..)`.
- Send empty events to `Monitor.checkinitialised()` and `Monitor.checkOpenSession()` respectively.
 
### 2. **No account may end up with a negative balance after being accessed.**

The property states that no account may end up with a negative balance after being accessed. The property can be expressed as follows:

```java
*.withdraw(..) target (UserAccount a) | a.getBalance() < 0 → fail("P3 violated");
*.deposit(..) target (UserAccount a) | a.getBalance() < 0 → fail("P3 violated");
```
`Monitor Description`: the monitor checks the balance of the account after a withdrawal or deposit action to ensure that it is always positive. A trace containing a withdrawal or deposit on an account with a subsequent negative balance violates the property. Otherwise, the trace satisfies the property.

`Instrumentation Description`: (TODO)
- Capture before method calls to `* UserAccount.withdraw(..)` and `* UserAccount.deposit(..)`
- Extract the method receiver object using BISM dynamic context object `getMethodReceiver`
- Send events to `Monitor.checkWithdraw(transactionsystem.UserAccount)` and `Monitor.checkDeposit(transactionsystem.UserAccount)` respectively.
 
### 3. **An account approved by the administrator may not have the same account number as any other existing account in the system.**

The property states that an account approved by the administrator may not have the same account number as any other already existing account in the system. The property can be expressed as follows:

```java
*.approveOpenAccount(Integer uid, String acc_number) | Monitor.approvedAccounts.contains(acc_number) → fail("P4 violated");
*.approveOpenAccount(Integer uid, String acc_number) | → Monitor.approvedAccounts.add(acc_number);

```

`Monitor Description`: The monitor maintains a set for all account numbers. The first condition is checked first then if it passes, the account number will be added to the set. A trace containing two calls to approveOpenAccount with the same account number violates the property.

`Instrumentation Description`: (TODO)
-  Capture before method calls to the `* *.approveOpenAccount(..)`  
-  Extract a list of method arguments using BISM dynamic context object `getAllMethodArgs`
-  Send the event to `Monitor.checkApproveOpenAccount(jList)`.
  

### 4. **Reactivation in the financial transaction system**

The property states that once a user is disabled, he or she may not withdraw from an account until activated again. The property can be specified as follows:

```java
UserInfo.makeDisabled(..) target (UserInfo u) | → Monitor.disabledUsers.add(u); 
UserInfo.makeActive(..) target (UserInfo u)| → Monitor.disabledUsers.remove(u); 
UserInfo.withdrawFrom(..) target (UserInfo u) | (Monitor.disabledUsers.contains(u)) →  fail(“P5 violated”);
```

`Monitor Description`: The monitor keeps track of disabled users by adding the user to a set upon being disabled and removing the user upon activating. Subsequently, upon withdrawal, the third rule ensures that the user is not on the list. Thus, a trace containing a call to makeDisabled followed by a withdrawal on the same user violates the property. On the contrary, a user performing a withdrawal after being disabled but later activated satisfies the property.

`Instrumentation Description`: (TODO)
- Capture before method calls to `* *.UserInfo.makeDisabled(..)`, `* *.UserInfo.makeActive(..)` and `UserInfo.withdrawFrom(..)`.
- Extract the method receiver object using BISM dynamic context object `getMethodReceiver`
- Send events to `Monitor.checkMakeDisabledUser(transactionsystem.UserInfo)`, `Monitor.checkMakeActiveUser(transactionsystem.UserInfo)` and `Monitor.checkWithdrawFrom(transactionsystem.UserInfo)` respectively.


 ## Your Tasks

Based on the group you were assigned to, you will use either the API or the DSL for different properties. Follow the instructions below according to your group assignment:

- **Group A**:
    - Use the API for Properties 1 and 3.
    - Use the DSL for Properties 2 and 4.
- **Group B**:
    - Use the DSL for Properties 1 and 3.
    - Use the API for Properties 2 and 4.

For each exercise, you are required to implement a single file within the exercise folder that specifies the instrumentation for that exercise. Total of 4 files. The name of this file should be `MyTransformer.java` for API mode and `transformer.spec` for DSL mode.

## Metrics

For each task and property, you are required to collect the following metrics:

- Time in minutes taken to write the transformer with BISM API and DSL.
- List of mistakes/errors encountered while writing the instrumentation that required recompilation and another run. Or just report the number.
- Overall time in minutes taken to complete the monitoring task.
- Rate the difficulty of the exercise on a scale from 1 to 5, where 1 = very easy and 5 = very hard.

| | Property 1 | Property 2 | Property 3 | Property 4 |
| --- | --- | --- | --- | --- |
| Time taken to write instrumentation (BISM API) |  |  |  |  |
| Time taken to write instrumentation (DSL) |  |  |  |  |
| List of Mistakes (BISM API) |  |  |  |  |
| List of Mistakes (DSL) |  |  |  |  |
| Time taken to finish complete task (BISM API) |  |  |  |  |
| Time taken to finish complete task (DSL) |  |  |  |  |
| Ease of use (BISM API) |  |  |  |  |
| Ease of use (DSL) |  |  |  |  |

Please fill in the table above with your results and email them to me at chukri.a.soueidi@inria.fr.


## Begin the Exercise

Navigate to the **[exercises](./exercises/README.md)** folder to start working on the exercise.
