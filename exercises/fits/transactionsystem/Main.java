package transactionsystem;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Scenarios.runAllScenarios();
        System.out.println("Finished running FiTS application");
       
        try {
            // Load the class by name
            Class<?> myClass = Class.forName("Monitor");
        
            // Get a reference to the static method
            Method myStaticMethod = myClass.getMethod("printStats");
        
            // Call the static method with a parameter
            myStaticMethod.invoke(null);
        } catch (ClassNotFoundException e) {
            // Handle class not found exception
        } catch (NoSuchMethodException e) {
            // Handle no such method exception
        } catch (IllegalAccessException e) {
            // Handle illegal access exception
        } catch (InvocationTargetException e) {
            // Handle invocation target exception
        }

        
    }

}
