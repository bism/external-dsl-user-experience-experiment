pointcut pc1 after MethodCall(* *.*List.iterator(..))

event e1("create",getMethodReceiver,getMethodResult) on pc1 

monitor m1{
    class: Monitor,
    events: [e1 to receiveEvent(String, Object, java.util.Iterator)]
}    