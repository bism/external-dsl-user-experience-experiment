import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;

public class MyTransformer extends Transformer {
 @Override
 public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc){
    // Filter to only method of interest
    if (mc.methodName.equals("iterator") && mc.methodOwner.contains("List")) {
       
        // Access to dynamic data, mc is needed so BISM can extract dynamic context objects
        DynamicValue list = dc.getMethodReceiver(mc);
        DynamicValue iterator = dc.getMethodResult(mc);   
       
      
        // Invoke the monitor after passing arguments
        StaticInvocation sti = 
            new StaticInvocation("Monitor", "receiveEvent");
        sti.addParameter("create");          
        sti.addParameter(list);
        sti.addParameter(iterator);
        invoke(sti);
    }
 } 
}